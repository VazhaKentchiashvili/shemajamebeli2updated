package com.vazhasapp.tbcshemajamebeli2updated

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class User(
    var fistName: String,
    var lastName: String,
    var email: String,
    var age: Int,
): Parcelable
