package com.vazhasapp.tbcshemajamebeli2updated

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.vazhasapp.tbcshemajamebeli2updated.databinding.ActivityUserDetailsBinding

class UserDetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityUserDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUserDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupUserDetailsOnScreen()
    }

    private fun setupUserDetailsOnScreen() {
        binding.apply {
            val selectedUser = intent.getParcelableExtra<User>(MainActivity.MY_LIST_KEY)
            tvFirstName.text = selectedUser!!.fistName
            tvLastName.text = selectedUser!!.lastName
            tvEmail.text = selectedUser!!.email
            tvAge.text = selectedUser!!.age.toString()
        }
    }
}