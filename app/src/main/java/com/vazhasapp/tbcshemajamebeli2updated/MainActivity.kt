package com.vazhasapp.tbcshemajamebeli2updated

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.Toast
import com.vazhasapp.tbcshemajamebeli2updated.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val userDummyData = mutableMapOf<String, User>()
    var deletedMemberCounter = 0
    var activeMemberCounter = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
    }

    private fun init() {
        binding.btnAddUser.setOnClickListener {
            if (checkEnteredValuesIsNullOrNot())
                addUser()
        }
        binding.btnRemoveUser.setOnClickListener {
            if (checkEnteredValuesIsNullOrNot())
                removeUser()
        }
        binding.btnUpdateUser.setOnClickListener {
            if (checkEnteredValuesIsNullOrNot())
                updateUser()
        }
        binding.btnShowUserDetails.setOnClickListener {
            val firstName = binding.etFirstName.text.toString().trim()
            val lastName = binding.etLastName.text.toString().trim()
            val email = binding.etEmail.text.toString().trim()
            val age = binding.etAge.text.toString().trim()

            if (checkEnteredValuesIsNullOrNot()) {
                var currentUser = User(firstName,lastName,email,age.toInt())
                val myIntent = Intent(this, UserDetailsActivity::class.java)
                myIntent.putExtra(MY_LIST_KEY, currentUser)
                startActivity(myIntent)
            }
        }
    }

    private fun addUser() {
        val firstName = binding.etFirstName.text.toString().trim()
        val lastName = binding.etLastName.text.toString().trim()
        val email = binding.etEmail.text.toString().trim()
        val age = binding.etAge.text.toString().trim()

        if (checkUserExistOrNot()) {
            userDummyData[email] = User(firstName, lastName, email, age.toInt())
            activeMemberCounter += 1
            Toast.makeText(this, "User added successfully", Toast.LENGTH_SHORT).show()
            increaseActiveMembers(activeMemberCounter)
            responseSuccessStatus()
        } else {
            Toast.makeText(this, "User already exist", Toast.LENGTH_SHORT).show()
            responseErrorStatus()
        }
    }

    private fun removeUser() {
        val email = binding.etEmail.text.toString()

        if (checkUserExistOrNot()) {
            Toast.makeText(this,
                "User doesn't exist", Toast.LENGTH_SHORT).show()
            responseErrorStatus()
        } else {
            userDummyData.remove(email)
            deletedMemberCounter += 1
            increaseDeletedMembers(deletedMemberCounter)
            Toast.makeText(this,
                "User deleted successfully", Toast.LENGTH_SHORT).show()
            responseSuccessStatus()
        }
    }

    private fun updateUser() {
        val firstName = binding.etFirstName.text.toString().trim()
        val lastName = binding.etLastName.text.toString().trim()
        val email = binding.etEmail.text.toString().trim()
        val age = binding.etAge.text.toString().trim()

        if (userDummyData.containsKey(email)) {
            userDummyData[email] = User(firstName,lastName,email,age.toInt())
            Toast.makeText(this, "User updated successfully", Toast.LENGTH_SHORT).show()
            responseSuccessStatus()
        } else {
            Toast.makeText(this, "User doesn't exist", Toast.LENGTH_SHORT).show()
            responseErrorStatus()
        }
    }

    // Check entered value is null or not, called always before REMOVE, ADD or UPDATE user!
    private fun checkEnteredValuesIsNullOrNot(): Boolean {
        val firstName = binding.etFirstName.text.toString()
        val lastName = binding.etLastName.text.toString()
        val email = binding.etEmail.text.toString()
        val age = binding.etAge.text.toString()

        return if (
            firstName.isBlank() ||
            lastName.isBlank() ||
            email.isBlank() ||
            age.isBlank()
        ) {
            Toast.makeText(this,
                "Please fill all the required fields", Toast.LENGTH_SHORT).show()
            false
        } else
            true
    }

    // Check user exist or not, called always before ADD, REMOVE or UPDATE user!
    private fun checkUserExistOrNot(): Boolean {
        val email = binding.etEmail.text.toString()
        return !userDummyData.containsKey(email)
    }

    // Counting deleted and activate users number.
    private fun increaseActiveMembers(counter: Int) {
        binding.tvActivateCounter.text = counter.toString()
    }

    private fun increaseDeletedMembers(counter: Int) {
        binding.tvDeletedCounter.text = counter.toString()
    }

    // Show text on display which tells your operation status
    private fun responseSuccessStatus() {
        binding.tvProcessResponse.apply {
            text = "Success"
            setTextColor(Color.GREEN)
            visibility = View.VISIBLE
        }
    }

    private fun responseErrorStatus() {
        binding.tvProcessResponse.apply {
            text = "Error"
            setTextColor(Color.RED)
            visibility = View.VISIBLE
        }
    }

    companion object {
        const val MY_LIST_KEY: String = "myList"
    }
}